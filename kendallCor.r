# 2018-05-04
# Ludmila Danilova
# correlation of promoter/enhancer methylation and gene expression by Kendall tau

library(Kendall)

tumSampExpr = rownames(sampAnnot)[which(sampAnnot[,'class']=='T')]
normSampExpr = rownames(sampAnnot)[which(sampAnnot[,'class']=='N')]
allSamp = c(tumSampExpr,normSampExpr)

# calculates Kendall correlation between methylation and expression
getTau = function(meth, expr)
{
	if (all(meth== 0) || sum(meth != 0) == 1)
	{
		return(c(AvgMeth = mean(meth, na.rm = T), cor = NA,pVal = NA))
	}else{
		Kendall <- Kendall(expr, meth)
		return(c(AvgMeth = mean(meth, na.rm = T), cor =Kendall$tau, pVal = Kendall$sl))
	}
}
# get correlations for promoter methylation
promMethCorPval = t(sapply(uniqueTargetGenes,function(g) getTau(promoterMeth[g,sampAnnot[allSamp,'dnaID']], rnaData[g,allSamp])))
colnames(promMethCorPval) = c('AvgPromMeth','PromoterCor','PromoterP')
promMethCorPval = cbind(promMethCorPval, PromoterFDR = p.adjust(promMethCorPval[,'PromoterP'], method = "BH"))

# get correlations for enhancer methylation and targeted genes
getTauEnh = function(enh, allGenes, methData, exprData)
{
	genes = intersect(rownames(exprData),allGenes[[enh]])
	res = cbind(enhacer = rep(enh,length(genes)),gene = genes,t(sapply(genes, function(g) getTau(methData[enh,], exprData[g,]))))
	return(res)
}

# take only enhancers that are significantly differentially methylated
signEnh = rownames(dmTable)[which(as.numeric(as.character(dmTable[,'wilcox.FDR'])) < dmFDR)]

enhMethCorPval = sapply(signEnh, getTauEnh, allTargetGenes, ehnancerMeth[, sampAnnot[allSamp,'dnaID']], rnaData[,allSamp])
enhMethCorPval = Filter(ncol, enhMethCorPval)
enhMethCorPval = do.call('rbind',enhMethCorPval)
colnames(enhMethCorPval) = c('enhancer','gene','AvgEnhMeth','EnhancerCor','EnhancerP')
enhMethCorPval = cbind(enhMethCorPval, EnhancerFDR = p.adjust(enhMethCorPval[,'EnhancerP'], method = "BH"))





