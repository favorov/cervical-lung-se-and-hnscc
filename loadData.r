# 2018-07-10
# Ludmila Danilova
# getting all necessary data to run the pipeline

# install differential.coverage, the package to work with MBDseq data
# devtools::install_github("favorov/differential.coverage")
library(differential.coverage)
library(TxDb.Hsapiens.UCSC.hg19.knownGene)
library(org.Hs.eg.db)

geti = function(x,i) x[i]

#=========================================
# promoter methylation
#=======================================
# get promoter info for all known genes
genelist<-genes(TxDb.Hsapiens.UCSC.hg19.knownGene)
geneSymbols <- select(
  org.Hs.eg.db,
  keys=as.character(names(genelist)),
  columns=c('SYMBOL'),
  keytype='ENTREZID'
)
names(genelist) = geneSymbols[,2]
promoters<-promoters(genelist,upstream=tssUp, downstream=tssDown)
# load methylation data and extract coverage for promoters
data.folder<-'./data'
source('./read_clinical.R')
source('./prepare_beds_and_contrast.R')
promoterMeth<-count.coverage.of.noodles(promoters,bedfilenames,DNAids)
rownames(promoterMeth) = names(promoters)
# remove comma from colnames
colnames(promoterMeth) = gsub(', ','',colnames(promoterMeth))

#====================================
# enhancer methylation
#====================================
enhancers <- read.csv("5CellLines_SEs.csv", header=TRUE, sep=",",quote="",na.strings="", comment.char="", stringsAsFactors = FALSE)
rownames(enhancers) = paste0(enhancers[,1], ":", enhancers[,2], "-", enhancers[,3])

enhancersGr = makeGRangesFromDataFrame(data.frame(enhancers[,1:3]))
seqinfo(enhancersGr)=seqinfo(genelist)
ehnancerMeth<-count.coverage.of.noodles(enhancersGr,bedfilenames,DNAids)
rownames(ehnancerMeth) = names(enhancersGr)
# remove comma from colnames
colnames(ehnancerMeth) = gsub(', ','',colnames(ehnancerMeth))
	 
#===========================
# RNAseq data
#===========================	 
rnaData = read.table(file = 'GSE112026_HPVOP_RSEMNorm.txt', header = T, row.names = 1, stringsAsFactors = F)
rnames = sapply(strsplit(rownames(rnaData),"\\|"),geti,1)
rnaData = rnaData[!duplicated(rnames),]
rnaData = t(do.call('rbind',rnaData))
rnaData = log2(rnaData+1)
rownames(rnaData) = rnames[!duplicated(rnames)]

#=================
# annotation data
#=================
sampAnnot = read.table('sampleID_mapping.txt', sep = '\t', header = T, row.names = 1, stringsAsFactors = F)

#=========================
# get target genes for enhancers +/-1Mb
#=========================
targetGenes = genes.with.TSS.covered.by.interval(enhancersGr, flanks = seFlank, genome.id = "hg19")
#targetGenes = data.frame(targetGenes)
# get target genes per enhancer
allTargetGenes = sapply(targetGenes$overlapped.TSS, function(x) unlist(strsplit(x,', ')))
allTargetGenesPos = sapply(targetGenes$overlapped.pos, function(x) unlist(strsplit(x,', ')))
names(allTargetGenes) = names(allTargetGenesPos) = names(enhancersGr)
allTargetGenesPos = sapply(names(allTargetGenes), function(e) as.numeric(allTargetGenesPos[[e]]))
allTargetGenesPos = sapply(names(allTargetGenes), function(e) setNames(allTargetGenesPos[[e]], allTargetGenes[[e]]))

# all target genes
uniqueTargetGenes = intersect(intersect(rownames(rnaData),unique(unlist(allTargetGenes,use.names = F))), rownames(promoterMeth)) 






 
