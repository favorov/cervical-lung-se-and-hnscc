# 2018-05-03
# Ludmila Danilova
# differential methylation of enhancers by Wilcoxon

tumSampMeth = intersect(colnames(ehnancerMeth),sampAnnot[which(sampAnnot[,'class']=='T'),'dnaID'])
normSampMeth = intersect(colnames(ehnancerMeth),sampAnnot[which(sampAnnot[,'class']=='N'),'dnaID'])

#Perform Wilcoxon for differential enhancer methylation between tumors and normals
wilcoxPval <- sapply(1:nrow(ehnancerMeth), function(i) wilcox.test(ehnancerMeth[i,tumSampMeth],ehnancerMeth[i,normSampMeth])$p.value)
tumor_mean_meth = apply(ehnancerMeth[,tumSampMeth],1,mean, na.rm = T)
norm_mean_meth = apply(ehnancerMeth[,normSampMeth],1,mean, na.rm = T)
hyperHypo = sapply(1:length(tumor_mean_meth), function(i) if(tumor_mean_meth[i] > norm_mean_meth[i]) return('hyper') else return('hypo'))
dmTable = cbind(wilcox.p.value = wilcoxPval, wilcox.FDR = p.adjust(wilcoxPval, method = 'BH'), meth.status = hyperHypo)
rownames(dmTable) = rownames(ehnancerMeth)
# save enhancer table
write.table(cbind(enhancers[,c('chr','start','end','Cell.line')],dmTable[rownames(enhancers),]), file = 'enhancerTable.txt', sep= '\t',quote = F)

  
