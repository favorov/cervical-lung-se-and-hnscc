# 2018-05-04
# Ludmila Danilova
# main script to run the pipeline

#==================
# parameters

# interval for promoter methylation
tssUp = 1500
tssDown = 500
# range of SE region +/- seFlank to find target genes
seFlank = 1e+6
# significance thresholds 
dmFDR = 0.05
corFDR = 0.05

#=============================
# run scripts
# loads all necessary data
source('loadData.r')
# calculates differential enhancer methylation
source('diffMeth.r')
# calculates correlation of promoter/enhancer methylation and gene expression by Kendall tau
source('kendallCor.r')

#=========================
tumSampExpr = rownames(sampAnnot)[which(sampAnnot[,'class']=='T')]
normSampExpr = rownames(sampAnnot)[which(sampAnnot[,'class']=='N')]
# get average expression in tumors and normal and direction of expression in cancer
geSummary = cbind(normal_mean_expr = apply(rnaData[,normSampExpr],1,mean, na.rm = T),
	tumor_mean_expr = apply(rnaData[,tumSampExpr],1,mean, na.rm = T))
geSummary = cbind(geSummary, Expr_in_cancer = apply(geSummary,1, function(x) if(x[1]<x[2])return('up')else return('down')))

# combine all data and create BigGeneTable
BigGeneTable = cbind(enhMethCorPval, # correlation of enhancer meth and gene expr
	dmTable[enhMethCorPval[,'enhancer'], ], # differential methylation of enhancers
	data.frame(promMethCorPval)[enhMethCorPval[,'gene'],], # correlation of promoter meth and gene expr
	geSummary[enhMethCorPval[,'gene'],]) # differential expression
#===================
# subset big table
# significantly differentially methylated SE
hyperBigTable = BigGeneTable[which(BigGeneTable[,'meth.status'] == 'hyper'& as.numeric(as.character(BigGeneTable[,'wilcox.FDR']))< dmFDR),]
hypoBigTable = BigGeneTable[which(BigGeneTable[,'meth.status'] == 'hypo'& as.numeric(as.character(BigGeneTable[,'wilcox.FDR']))< dmFDR),]

# find pairs that have FDR < 0.05 (significant Kendall tau) and negative correlation
hyperBigTable_05 = hyperBigTable[which(as.numeric(as.character(hyperBigTable[, 'EnhancerFDR'])) < corFDR & 
	as.numeric(as.character(hyperBigTable$EnhancerCor)) < 0),]
hypoBigTable_05 = hypoBigTable[which(as.numeric(as.character(hypoBigTable[, 'EnhancerFDR'])) < corFDR &
	as.numeric(as.character(hypoBigTable$EnhancerCor)) < 0),]
	
# final set of SE-pairs to validate 
write.table(rbind(hyperBigTable_05,hypoBigTable_05), file='geneEnhancerPairs_toValidate.txt', sep="\t", row.names = F)












